#[macro_use]
extern crate lazy_static;

use std::path::PathBuf;

use ::log::*;
use actix_files::Files;
use actix_web::{guard, middleware, web, App, HttpServer};
use failure::Fallible;
use structopt::StructOpt;

mod extensions;
mod log;

const MAX_EXT_SIZE: usize = 10 * 1024 * 1024;

pub struct BaseURL(String);

fn main() -> Fallible<()> {
    let opt = Opt::from_args();

    crate::log::init(&opt.log_level, &opt.log_file)?;
    info!("Extension server");

    let dir = opt.directory.clone();
    let store = extensions::init_store(opt.directory)?;
    let base_url = opt.base_url;

    HttpServer::new(move || {
        App::new()
            .data(store.clone())
            .data(BaseURL(base_url.clone()))
            .wrap(middleware::Logger::default())
            .service(web::resource("").route(web::get().to(extensions::list_handler)))
            .service(web::resource("/").route(web::get().to(extensions::list_handler)))
            .service(
                web::resource("/updates.json").route(web::get().to(extensions::manifest_handler)),
            )
            .service(
                web::resource("/upload/{name}@{namespace}/{version}").route(
                    web::post()
                        .guard(guard::Header("Content-Type", "application/x-xpinstall"))
                        .data(web::PayloadConfig::default().limit(MAX_EXT_SIZE))
                        .to_async(extensions::upload_handler),
                ),
            )
            .service(Files::new("/extensions", &dir))
    })
    .bind(opt.listen)?
    .run()?;

    Ok(())
}

#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct Opt {
    #[structopt(
        long,
        default_value = "warn",
        raw(possible_values = r#"&["error", "warn", "info", "debug", "trace"]"#)
    )]
    /// Log level
    pub log_level: String,

    #[structopt(long)]
    /// Path to log file, defaults to stderr
    pub log_file: Option<PathBuf>,

    #[structopt(long, default_value = "127.0.0.1:3000")]
    /// IP and port to listen on
    pub listen: String,

    #[structopt(name = "BASE_URL")]
    /// Base URL where the service is running
    pub base_url: String,

    #[structopt(name = "DIR")]
    /// Directory where the extensions are stored
    pub directory: PathBuf,
}
