use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use failure::Fallible;
use log::*;

use super::Extension;

pub type Store = Arc<Mutex<InternalStore>>;

#[derive(Debug)]
pub struct InternalStore {
    pub dir: PathBuf,
    pub extensions: Vec<Extension>,
}

pub fn init_store(dir: PathBuf) -> Fallible<Store> {
    info!("Initializing store at {:?}", dir);

    let extensions: Vec<Extension> = std::fs::read_dir(&dir)?
        .filter_map(Result::ok)
        .filter_map(|e| e.file_name().into_string().ok())
        .filter_map(|e| e.parse().ok())
        .collect();

    debug!("{:#?}", extensions);

    Ok(Arc::new(Mutex::new(InternalStore { dir, extensions })))
}
