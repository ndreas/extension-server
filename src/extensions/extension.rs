use std::str::FromStr;

use regex::Regex;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Extension {
    pub name: String,
    pub namespace: String,
    pub version: String,
}

impl Extension {
    pub fn id(&self) -> String {
        format!("{}@{}", self.name, self.namespace)
    }

    pub fn filename(&self) -> String {
        format!("{}@{}-{}.xpi", self.name, self.namespace, self.version)
    }
}

lazy_static! {
    static ref FILENAME_FORMAT: Regex = Regex::new(
        r"(?x)
        ^
        (?P<name>.*?)
        @
        (?P<namespace>.*)
        -
        (?P<version>.*?)
        \.xpi$"
    )
    .unwrap();
}

impl FromStr for Extension {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let cap = FILENAME_FORMAT.captures(s).ok_or(())?;

        Ok(Extension {
            name: match_to_string(cap.name("name"))?,
            namespace: match_to_string(cap.name("namespace"))?,
            version: match_to_string(cap.name("version"))?,
        })
    }
}

fn match_to_string(m: Option<regex::Match>) -> Result<String, ()> {
    Ok(String::from(m.ok_or(())?.as_str()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_filename() {
        let ext = Extension {
            name: String::from("foo"),
            namespace: String::from("example.com"),
            version: String::from("1.23"),
        };
        assert_eq!(ext.filename(), "foo@example.com-1.23.xpi");
    }

    #[test]
    fn test_parse_str() {
        let res = Extension::from_str("foo@example.com-1.23.xpi");
        assert!(res.is_ok());
        let ext = res.unwrap();

        assert_eq!(ext.name, "foo");
        assert_eq!(ext.namespace, "example.com");
        assert_eq!(ext.version, "1.23");
    }

    #[test]
    fn test_parse_domain_with_dash() {
        let res = Extension::from_str("foo@example-foo.com-1.23.xpi");
        assert!(res.is_ok());
        let ext = res.unwrap();

        assert_eq!(ext.name, "foo");
        assert_eq!(ext.namespace, "example-foo.com");
        assert_eq!(ext.version, "1.23");
    }

    #[test]
    fn test_parse_invalid_str() {
        assert!(Extension::from_str("zomg.xpi").is_err());
        assert!(Extension::from_str("foo@bar.xpi").is_err());
        assert!(Extension::from_str("foo@example.com-1.23.zip").is_err());
    }
}
