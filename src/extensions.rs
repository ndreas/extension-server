use std::collections::HashMap;
use std::fs;
use std::io::Write;

use actix_web::{web, Error, HttpResponse};
use askama::Template;
use futures::{Future, Stream};
use log::*;
use serde::Serialize;

mod extension;
use extension::Extension;

mod store;
pub use store::init_store;
use store::Store;

#[derive(Template)]
#[template(path = "index.html.j2")]
struct IndexTemplate<'a> {
    pub extensions: &'a Vec<Extension>,
}

pub fn list_handler(s: web::Data<Store>) -> Result<HttpResponse, HttpResponse> {
    let store = s.lock().map_err(err_response)?;
    let tpl = IndexTemplate {
        extensions: &store.extensions,
    };
    Ok(HttpResponse::Ok().body(tpl.render().map_err(err_response)?))
}

pub fn manifest_handler(
    s: web::Data<Store>,
    base_url: web::Data<crate::BaseURL>,
) -> Result<HttpResponse, HttpResponse> {
    let store = s.lock().map_err(err_response)?;
    let mut addons: HashMap<String, Addon> = HashMap::new();

    for ext in store.extensions.iter() {
        addons
            .entry(ext.id())
            .and_modify(|a| a.updates.push(ExtensionLink::new(&base_url.0, ext)))
            .or_insert(Addon {
                updates: vec![ExtensionLink::new(&base_url.0, ext)],
            });
    }

    Ok(HttpResponse::Ok().json(Manifest { addons }))
}

pub fn upload_handler(
    s: web::Data<Store>,
    extension: web::Path<Extension>,
    body: web::Payload,
) -> impl Future<Item = HttpResponse, Error = Error> {
    debug!("Received extension: {:?}", extension);
    body.map_err(Error::from)
        .fold(web::BytesMut::new(), |mut body, chunk| {
            body.extend_from_slice(&chunk);
            Ok::<_, Error>(body)
        })
        .and_then(move |body| {
            let mut store = s.lock().map_err(err_response)?;

            let mut path = store.dir.clone();
            path.push(extension.filename());

            let mut file = fs::File::create(path).map_err(err_response)?;
            file.write_all(body.as_ref()).map_err(err_response)?;

            store.extensions.push(extension.into_inner());

            Ok(HttpResponse::Ok().finish())
        })
}

fn err_response(err: impl std::error::Error) -> HttpResponse {
    error!("{}", err);
    HttpResponse::InternalServerError().finish()
}

#[derive(Serialize)]
struct Manifest {
    pub addons: HashMap<String, Addon>,
}

#[derive(Serialize)]
struct Addon {
    pub updates: Vec<ExtensionLink>,
}

#[derive(Serialize)]
struct ExtensionLink {
    pub version: String,
    pub update_link: String,
}

impl ExtensionLink {
    fn new(base_url: &str, ext: &Extension) -> ExtensionLink {
        ExtensionLink {
            version: ext.version.clone(),
            update_link: format!("{}/extensions/{}", base_url, ext.filename()),
        }
    }
}
