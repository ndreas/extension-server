use std::fs::OpenOptions;
use std::path::PathBuf;

use chrono::Local;
use failure::Fallible;
use fern::colors::{Color, ColoredLevelConfig};
use log::LevelFilter;
use reopen::Reopen;

pub fn init(level: &str, logfile: &Option<PathBuf>) -> Fallible<()> {
    let mut logger = fern::Dispatch::new();

    logger = logger.level(match level {
        "error" => LevelFilter::Error,
        _ => LevelFilter::Warn,
    });

    let level = match level {
        "error" => LevelFilter::Error,
        "info" => LevelFilter::Info,
        "debug" => LevelFilter::Debug,
        "trace" => LevelFilter::Trace,
        _ => LevelFilter::Warn,
    };

    logger = logger.level_for("extension_server", level);
    logger = logger.level_for("actix_web", level);

    logger = if let Some(path) = logfile {
        let path = path.clone();
        let file = Reopen::new(Box::new(move || {
            OpenOptions::new()
                .create(true)
                .write(true)
                .append(true)
                .open(path.clone())
        }))?;

        file.handle().register_signal(libc::SIGHUP)?;

        let boxed_file: Box<dyn std::io::Write + std::marker::Send + 'static> = Box::new(file);

        logger
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "{} [{}] [{}] {}",
                    Local::now().format("%+"),
                    record.level(),
                    record.target(),
                    message
                ))
            })
            .chain(boxed_file)
    } else {
        let color = ColoredLevelConfig::new()
            .error(Color::Red)
            .warn(Color::Yellow)
            .info(Color::Green)
            .debug(Color::White)
            .trace(Color::BrightBlack);

        logger
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "{} [{}] [{}] {}",
                    Local::now().format("%+"),
                    color.color(record.level()),
                    record.target(),
                    message
                ))
            })
            .chain(std::io::stderr())
    };

    logger.apply()?;

    Ok(())
}
